
#Motor Vehicle Collisions - Crashes

## Question pricipale concernée par l'étude:
Comment le moment, le lieu, le facteur d'accident, et le type de véhicule impliqué influencent-ils la mortalité et la fréquence des accidents de la route ?

### Autres questions nécessaires pour répondre à la problématique :
- Quels sont les types de véhicules causant le plus d'accidents 

- Quelle est l'heure la plus meurtière ? 
    - Existe-il un lien entre le nombre de morts et l'heure de l'accident ? 

- Quelle est l'influence de type de la rue sur le facteur de l'accident ? 
    - Pour le choix du type de la rue : AVENUE et STREET.
        - Je considère une rue comme STREET si son nom contient ou pas le mot clé 'STREET' et comme 'AVENUE' si son nom contient le mot clé 'AVENUE'
    
    1. Calculer le nombre de STREET et AVENUE à NYC. 
    2. Calculer le nombre d'accident par types de rues (AVENUE & STREET)
    3. Chercher les 5 facteurs d'accidents les plus récurrents par type de rue (AVENUE ou STREET) et les catégoriser.
    
    * Conclusion (corélation):
    Trouver une corélation entre le facteur de l'accident de chaque type de rue et ceux qui apparaissent ou pas dans l'autre type avec le type de la rue.
    Justifications possibles et liaison avec l'année de l'accident : 
        * Création massive des panneaux de signalisation durant telle ou telle année.
        * Les AVENUES autorisent une vitesse plus élévée par rapport aux streets.
        * Autres décisions politiques du ministères des transports à NYC.
        * Justifier la présence  d'un grand nombre  d'AVENUES ou de STREET et son impact sur les 5 facteurs d'accidents les plus récurrents.
     




**Source of the dataset :**(data.gov) https://catalog.data.gov/dataset/motor-vehicle-collisions-crashes 
 




