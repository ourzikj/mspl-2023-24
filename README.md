# MSPL-2032-2024

Gitlab du cours de MIAGE-3 Modèles Statistiques et Programmation Lettrée

Équipe pédagogique :
- Jean-Marc.Vincent@univ-grenoble-alpes.fr
- Danilo.Carastan-dos-Santos@univ-grenoble-alpes.fr

Organisation (prévision):
pour les salles consulter ADE

Le pad https://pads.univ-grenoble-alpes.fr/p/Miage3-mspl


1. 09/01 Introduction, Organisation  (JMV)
2. 16/01 Installation de l'environnement Git/RStudio exemples de Programmation Lettrée (DCS)
3. 23/01 Analyse de figures, checklist for good graphics (JMV)
4. 30/01 Data management dplyr (JMV) 
5. 06/02 Processus d'analyse et visualisation ggplot2 (DCS)
6. 13/02 Tests statistiques intervalles de confiance (JMV)
7. 20/02 Partiel + Projet (mise en place)  (JMV+DCS)

=== Interruption pédagogique

8. 05/03 Projet + éléments d'analyse statistique (DCS)
9. 12/03 Projet + éléments d'analyse statistique (JMV)
10. 19/03 Projet + éléments d'analyse statistique (DCS)
11. 26/03 Projet + éléments d'analyse statistique (JMV)
12. 02/04 Projet : présentations (JMV+DCS)


## Matériel utile
- [R for Data Science](https://r4ds.had.co.nz/)
- [RStudio IDE](https://raw.githubusercontent.com/rstudio/cheatsheets/main/rstudio-ide.pdf)
- [RStudio Markdown](https://www.rstudio.com/wp-content/uploads/2015/02/rmarkdown-cheatsheet.pdf)
- [RStudio Shortcuts](https://support.rstudio.com/hc/en-us/articles/200711853-Keyboard-Shortcuts-in-the-RStudio-IDE)

## Mini-projet
The Mini-Projet has to be developed in *teams of 4 students (at most)*.

### Important

You have to write a basic README file as soon as possible, following the [provided template](Projet/README_model.md).
You need to rename this file as `README_name1_name2.md`.

### Datasets

Non exhaustive list where data sets may be chosen from:

- FR Data.gouv: https://www.data.gouv.fr/fr/
- Insee: https://www.insee.fr/fr/accueil
- Kaggle: https://www.kaggle.com/datasets
- US Data.gov: https://catalog.data.gov/dataset
- Our World in Data: https://ourworldindata.org/

### Guidelines

1. Justify the data set selection and described it
2. Formulate three questions about the selected data set
3. Discuss with the professors and choose one question among these
4. Propose a methodology to answer that question
5. Implement this methodology using Literate Programming
6. The report must be acessible online (in your git repository)

### Report

The report has to be written with Rstudio, using R or Python. You need to provide both the original file
(`*.Rmd`) and a compiled file (`*.pdf` or `*.html`). Both files need to be placed in the `Projet` directory of
your git repository. The files must be named `projet_MSPL_name1_name2.extension` (for instance,
`projet_MSPL_Vincent_Cornebize.Rmd`). or probably on moodle

The report must contain:
- Frontpage
  - _Title_
  - Student's name
- Table of contents
- Introduction
  - Context / Dataset description
    - How the dataset has been obtained?
  - Description of the question
- Methodology
  - Data clean-up procedures
  - Scientific workflow
  - Data representation choices
- Analysis in Literate Programming
- Conclusion
- References

### Critères pour l'évaluation du projet

- *Question posée*
  - clarté de la question pas évident, plus de l'observation, niveau
    de diplôme
  - qualité de la réponse apportée : bonne exploration
  - ouverture 

- *Environnement de développement*
  - maîtrise de git
  - reproductibilité
  - maîtrise de R-studio

- *Statistiques*
  - maîtrise de R
  - adaptation des méthodes statistiques au problème 
  - qualité de l'analyse
 
- *Communication*
  - lisibilité et contenu du rapport
  - présentation structure
  - checklist for good graphics
  - qualité de la réponse aux questions

- *Méthodologie*
  - positionnement du problème Introduction 
  - description et analyse du jeu de données 
  - formulation de la question 
  - Nettoyage et workflow (reproductibilité) 
  - Choix de représentation 
  - Synthèses

### Important notes

The question might be from simple to complex. More the question is
simple, more deep should be the analysis.

### Groups

Your project defence will be composed of (strictly) 5 minutes of presentation
and 2 minutes of questions. The presentation support could be anything: if you
have a PDF, you have to provide it to us in advance, if you need your own
machine be prepared (setup time is part of your defence time).

| id | Group | Time  | Teams                                       |Theme |   Repository | Rapport
|-----|-------|-------|----------------------------------------------|---------------|-------------------------|-----
